hash: main.c
	gcc -c main.c
	gcc -static -o hash main.o ./libsha1.a
	#TODO: Compilación con linking estático

hash_dyn: main.c
	gcc -c main.c
	gcc -o hash_dyn main.o ./libsha1.so -Wl,-rpath,.
	#TODO: Compilación con linking dinámico
